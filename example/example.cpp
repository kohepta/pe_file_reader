﻿// example.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include <Windows.h>
#include <tchar.h>
#include <iostream>
#include "..\pefile\pefile.h"

int _tmain(int argc, _TCHAR* argv[])
{
	std::wcout.imbue( std::locale( "Japanese", std::locale::ctype ) );

	PeFile pe( L"c:\\windows\\system32\\kernel32.dll" );

	auto symbols = pe.getExportSymbols();

	std::cout.setf( std::ios::hex, std::ios::basefield );

	for ( auto it = symbols.begin(); it != symbols.end(); it ++ )
	{
		std::cout << "name: " << it->name << " -> 0x" 
			<< std::uppercase << it->address << std::endl;
	}

	return 0;
}
