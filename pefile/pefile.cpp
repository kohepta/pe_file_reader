﻿#include "pefile.h"
#include <imagehlp.h>
#include <iostream>

#pragma comment( lib, "imagehlp.lib" )

namespace
{

	DWORD ReadMemoryNoexcept(
		HANDLE processHandle,
		LPCVOID offset, 
		LPVOID buffer,
		DWORD size,
		DWORD * read )
	{
		DWORD error;
		DWORD r;

		error = ERROR_SUCCESS;
		r = 0;

		if ( 0 == ReadProcessMemory( processHandle,
				offset,
				buffer,
				size,
				&r ) )
		{
			error = GetLastError();
		}

		if ( NULL != read )
		{
			*read = r;
		}

		return error;
	}

	void ReadMemory(
		HANDLE processHandle,
		LPCVOID offset, 
		LPVOID buffer,
		DWORD size,
		DWORD * read = NULL )
	{
		DWORD error = ReadMemoryNoexcept(
			processHandle,
			offset,
			buffer,
			size,
			read );

		if ( ERROR_SUCCESS != error )
		{
			throw std::runtime_error( "[ERROR]/ReadMemory: Failed read memory !" );
		}
	}

	DWORD WriteMemoryNoexcept(
		HANDLE processHandle,
		LPVOID offset,
		LPVOID buffer,
		DWORD size,
		DWORD * wrote )
	{
		DWORD error;
		DWORD old;
		DWORD w;
	
		error = ERROR_SUCCESS;
		w = 0;

		if ( 0 == VirtualProtectEx( processHandle, offset, size, PAGE_READWRITE, &old ) )
		{
			error = GetLastError();
		}
		else
		{
			if ( 0 == WriteProcessMemory( processHandle, offset, buffer, size, &w ) )
			{
				error = GetLastError();
			}

			if ( 0 == VirtualProtectEx( processHandle, offset, size, old, &old ) )
			{
				error = GetLastError();
			}
		}

		if ( NULL != wrote )
		{
			*wrote = w;
		}

		return error;
	}

void WriteMemory(
	HANDLE processHandle,
	LPVOID offset,
	LPVOID buffer,
	DWORD size,
	DWORD * wrote = NULL )
{
	DWORD error = WriteMemoryNoexcept(
		processHandle,
		offset,
		buffer,
		size,
		wrote );

	if ( ERROR_SUCCESS != error )
	{
		throw std::runtime_error( "[ERROR]/WriteMemory: Failed write memory !" );
	}
}

}

//! コンストラクタ (ファイル名)
PeFile::PeFile(
	LPCTSTR fileName,
	DWORD originalBase ) 
: _mAllocateState( FLAT_ALLOCATE ),
  _mIsLoaded( false ),
  _mOriginalBase( NULL ),
  _mLoadBase( NULL ),
  _mLoadImportBase( NULL ),
  _mLoadExportBase( NULL ),
  _mLoadIatBase( NULL ),
  _mNtHeaders( NULL ),
  _mSectionTable( NULL )
{
	if ( fileName )
	{
	    // イメージファイルをメモリにマップする。
	    _mLoadBase = _mappingImage( fileName );

		if ( originalBase > 0 )
		{
			_mOriginalBase = reinterpret_cast< BYTE * >( originalBase );

			_mIsLoaded = true;
		}

	    if( _mLoadBase == NULL )
	    {
			std::cerr << "[ERROR]/PeFile(): PE file load error !" << std::endl;
	    }
		else
		{
			// NT ヘッダへのポインタを取得。
			_mNtHeaders = ImageNtHeader( _mLoadBase );
			
			// セクションテーブルへのポインタを取得。
			_mSectionTable = reinterpret_cast< IMAGE_SECTION_HEADER * >( _mNtHeaders + 1 );
			
			_mLoadImportBase = _getDirectoryEntry( IMAGE_DIRECTORY_ENTRY_IMPORT );
			
			_mLoadExportBase = _getDirectoryEntry( IMAGE_DIRECTORY_ENTRY_EXPORT );
			
			_mLoadIatBase = _getDirectoryEntry( IMAGE_DIRECTORY_ENTRY_IAT );

			if ( NULL == _mOriginalBase )
			{
				IMAGE_OPTIONAL_HEADER * optional;
	
				optional = getOptionalHeader();
	
				if ( NULL != optional )
				{
					_mOriginalBase = reinterpret_cast< BYTE * >( optional->ImageBase );
				}
			}
		}
	}
}

//! コンストラクタ (モジュールハンドル)
PeFile::PeFile( HMODULE hModule )
: _mAllocateState( NO_ALLOCATE ),
  _mIsLoaded( true ),
  _mOriginalBase( NULL ),
  _mLoadBase( NULL ),
  _mLoadImportBase( NULL ),
  _mLoadExportBase( NULL ),
  _mLoadIatBase( NULL ),
  _mNtHeaders( NULL ),
  _mSectionTable( NULL )
{
	if ( NULL != hModule )
	{
		// NT ヘッダの取得
		_mNtHeaders = ImageNtHeader( hModule );
		
		if ( NULL == _mNtHeaders )
		{
			std::cerr << "[ERROR]/PeFile(): Not PE file format !" << std::endl;
		}
		else
		{
			_mLoadBase = reinterpret_cast< BYTE * >( hModule );
			
			_mSectionTable = reinterpret_cast< IMAGE_SECTION_HEADER * >( _mNtHeaders + 1 );
			
			_mLoadImportBase = _getDirectoryEntry( IMAGE_DIRECTORY_ENTRY_IMPORT );
			
			_mLoadExportBase = _getDirectoryEntry( IMAGE_DIRECTORY_ENTRY_EXPORT );
			
			_mLoadIatBase = _getDirectoryEntry( IMAGE_DIRECTORY_ENTRY_IAT );
		}
	}
}

//! コンストラクタ (プロセスハンドル, ベースオフセット)
PeFile::PeFile(
	HANDLE processHandle,
	DWORD baseOffset )
: _mAllocateState( DISCRETE_ALLOCATE ),
  _mIsLoaded( true ),
  _mOriginalBase( NULL ),
  _mLoadBase( NULL ),
  _mLoadImportBase( NULL ),
  _mLoadExportBase( NULL ),
  _mLoadIatBase( NULL ),
  _mNtHeaders( NULL ),
  _mSectionTable( NULL )
{
	if ( INVALID_HANDLE_VALUE != processHandle && 0 != baseOffset )
	{
		LPVOID pBaseOffset = reinterpret_cast< LPVOID >( baseOffset );

		// 指定プロセスのモジュールイメージヘッダをメモリにマッピング
	    _mLoadBase = _mappingModuleImageHeader( processHandle, pBaseOffset );

	    if( _mLoadBase == NULL )
	    {
			std::cerr << "[ERROR]/PeFile(): PE file load error !" << std::endl;
		}
		else
		{
			// オリジナルベースアドレス保存
			_mOriginalBase = static_cast< BYTE * >( pBaseOffset );
			
			// NT ヘッダへのポインタを取得。
			_mNtHeaders = ImageNtHeader( _mLoadBase );
			
			// セクションテーブルへのポインタを取得。
			_mSectionTable = reinterpret_cast< IMAGE_SECTION_HEADER * >( _mNtHeaders + 1 );
			
			// インポートディレクトリを部分読み込み
			_mLoadImportBase = _mappingModuleImageDirectory(
				processHandle,
				pBaseOffset,
				_getDirectoryEntryRva( IMAGE_DIRECTORY_ENTRY_IMPORT ),
				_getDirectoryEntrySize( IMAGE_DIRECTORY_ENTRY_IMPORT ) );
			
			// エクスポートディレクトリを部分読み込み
			_mLoadExportBase = _mappingModuleImageDirectory(
				processHandle,
				pBaseOffset,
				_getDirectoryEntryRva( IMAGE_DIRECTORY_ENTRY_EXPORT ),
				_getDirectoryEntrySize( IMAGE_DIRECTORY_ENTRY_EXPORT ) );
			
			// IAT ディレクトリを部分読み込み
			_mLoadIatBase = _mappingModuleImageDirectory(
				processHandle,
				pBaseOffset,
				_getDirectoryEntryRva( IMAGE_DIRECTORY_ENTRY_IAT ),
				_getDirectoryEntrySize( IMAGE_DIRECTORY_ENTRY_IAT ) );
		}
	}
}

// デストラクタ
PeFile::~PeFile()
{
	switch( _mAllocateState )
	{
	case FLAT_ALLOCATE:
		VirtualFree( _mLoadBase, 0, MEM_RELEASE );
		break;
	case DISCRETE_ALLOCATE:
		VirtualFree( _mLoadBase, 0, MEM_RELEASE );
		VirtualFree( _mLoadImportBase, 0, MEM_RELEASE );
		VirtualFree( _mLoadExportBase, 0, MEM_RELEASE );
		VirtualFree( _mLoadIatBase, 0, MEM_RELEASE );
		break;
	default:
		break;
	}
	
	_mLoadBase = NULL;
	_mLoadImportBase = NULL;
	_mLoadExportBase = NULL;
	_mLoadIatBase = NULL;
}

//! ロードアドレス取得
BYTE * PeFile::getLoadBase() const
{
	return _mLoadBase;
}

BYTE * PeFile::getLoadImportBase() const
{
	return _mLoadImportBase;
}

BYTE * PeFile::getLoadExportBase() const
{
	return _mLoadExportBase;
}

BYTE * PeFile::getLoadIatBase() const
{
	return _mLoadIatBase;
}

//! イメージサイズ取得
DWORD PeFile::getImageSize() const
{
	DWORD size;
	
	size = 0;
	
	if ( getLoadBase() )
	{
		size = getOptionalHeader()->SizeOfImage;
	}
	
	return size;
}

//! NT ヘッダ取得
IMAGE_NT_HEADERS * PeFile::getNtHeaders() const
{
	return getLoadBase() ? _mNtHeaders : NULL;
}

//! ファイルヘッダ取得
IMAGE_FILE_HEADER * PeFile::getFileHeader() const
{
	return getLoadBase() ? &_mNtHeaders->FileHeader : NULL;
}

//! オプショナルヘッダ取得
IMAGE_OPTIONAL_HEADER * PeFile::getOptionalHeader() const
{
	return getLoadBase() ? &_mNtHeaders->OptionalHeader : NULL;
}

//! セクションヘッダ取得
IMAGE_SECTION_HEADER * PeFile::getSectionHeader( UINT index ) const
{
	IMAGE_SECTION_HEADER * sec;

	sec = NULL;

	if ( getLoadBase()
		&& 0 <= index 
		&& index < getFileHeader()->NumberOfSections )
	{
		sec = &_mSectionTable[index];
	}

	return sec;
}

//! セクションデータ取得
SectionDataContainer PeFile::getSectionData() const
{
	// NRVO is applied in release build.

	SectionDataContainer sections;
	IMAGE_FILE_HEADER * fileHeader;
	
	fileHeader = getFileHeader();
	
	if ( fileHeader )
	{
		for ( UINT i = 0; i < fileHeader->NumberOfSections; i ++ )
		{
			IMAGE_SECTION_HEADER * sectionHeader;
			BYTE tmp[IMAGE_SIZEOF_SHORT_NAME + 1] = { 0 };
			DWORD address;
			SectionData section = { 0 };
			
			sectionHeader = getSectionHeader( i );
			
			if ( _mIsLoaded )
			{
				address = ( _mOriginalBase )
					? reinterpret_cast< DWORD >( _mOriginalBase )
					: reinterpret_cast< DWORD >( getLoadBase() );
			}
			else
			{
				address = getOptionalHeader()->ImageBase;
			}
			
			section.address = address + sectionHeader->VirtualAddress;
			section.size = sectionHeader->Misc.VirtualSize;
			section.existInitData = ( sectionHeader->PointerToRawData > 0 ) ? true : false;

			memcpy( tmp, sectionHeader->Name, IMAGE_SIZEOF_SHORT_NAME );

			if ( sectionHeader->Name[IMAGE_SIZEOF_SHORT_NAME - 1] )
			{
				tmp[IMAGE_SIZEOF_SHORT_NAME] = 0;
			}

			section.name = reinterpret_cast< LPSTR >( tmp );
			
			sections.push_back( section );
		}
	}

	return sections;
}

//! 指定した名前のセクションデータを取得
SectionData PeFile::getSectionDataFromName( LPCSTR name ) const
{
	SectionDataContainer sections;
	SectionData data = { 0 };

	sections = getSectionData();

	for ( SectionDataContainer::iterator it = sections.begin();
		it != sections.end();
		it ++ )
	{
		if ( !strcmp( it->name.c_str(), name ) )
		{
			data = *it;
			break;
		}
	}

	return data;
}

//! EXE のテキストセクションデータを取得
SectionData PeFile::getExeTextSectionData() const
{
	IMAGE_OPTIONAL_HEADER * optional;
	DWORD address = 0;
	SectionData section = { 0 };
	
	optional = getOptionalHeader();
	
	if ( optional )
	{
		IMAGE_FILE_HEADER * fileHeader;
		DWORD entry;
		
		entry = optional->AddressOfEntryPoint;
		
		fileHeader = getFileHeader();
	
		if ( fileHeader )
		{
			for ( UINT i = 0; i < fileHeader->NumberOfSections; i ++ )
			{
				IMAGE_SECTION_HEADER * sectionHeader;
				BYTE tmp[IMAGE_SIZEOF_SHORT_NAME + 1] = { 0 };
				
				sectionHeader = getSectionHeader( i );
				
				if ( entry >= sectionHeader->VirtualAddress
					&& entry < sectionHeader->VirtualAddress + sectionHeader->Misc.VirtualSize )
				{
					address = sectionHeader->VirtualAddress;
					
					if ( _mIsLoaded )
					{
						address += ( _mOriginalBase )
							? reinterpret_cast< DWORD >( _mOriginalBase )
							: reinterpret_cast< DWORD >( getLoadBase() );
					}
					else
					{
						address += optional->ImageBase;
					}
					
					section.address = address;
					section.size = sectionHeader->Misc.VirtualSize;
					section.existInitData = true;
					
					memcpy( tmp, sectionHeader->Name, IMAGE_SIZEOF_SHORT_NAME );

					if ( sectionHeader->Name[IMAGE_SIZEOF_SHORT_NAME - 1] )
					{
						tmp[IMAGE_SIZEOF_SHORT_NAME] = 0;
					}

					section.name = reinterpret_cast< LPSTR >( tmp );
					
					break;
				}
			}
		}
	}

	return section;
}

//! インポート DLL 名の列挙を取得
strings PeFile::getImportDllNames() const
{
	// NRVO is applied in release build.

	strings dllNames;
	IMAGE_IMPORT_DESCRIPTOR * importDescs;

	importDescs = reinterpret_cast< IMAGE_IMPORT_DESCRIPTOR * >(
		getLoadImportBase() );

	if ( importDescs )
	{
		for ( UINT i = 0; 0 != importDescs[i].FirstThunk; i ++ )
		{
			string name;
			
			name = static_cast< LPSTR >( _getImportData( importDescs[i].Name ) );
			
			dllNames.push_back( name );
		}
	}

	return dllNames;
}

//! インポートしているシンボルの列挙を取得
ImportSymbolContainer PeFile::getImportSymbols( LPCSTR dllName ) const
{
	// NRVO is applied in release build.

	ImportSymbolContainer symbols;
	IMAGE_IMPORT_DESCRIPTOR * importDescs;

	importDescs = reinterpret_cast< IMAGE_IMPORT_DESCRIPTOR * >( getLoadImportBase() );
	
	if ( importDescs && ( importDescs[0].OriginalFirstThunk || !_mIsLoaded ) )
	{
		for ( UINT i = 0; 0 != importDescs[i].FirstThunk; i ++ )
		{
			LPCSTR name = static_cast< LPCSTR >( _getImportData( importDescs[i].Name ) );

			if ( 0 == _stricmp( dllName, name ) )
			{
				DWORD * iaTable = static_cast< DWORD * >(
					_getIatData( importDescs[i].FirstThunk ) );
				
				DWORD * inTable = importDescs[i].OriginalFirstThunk
					? static_cast< DWORD * >( _getImportData( importDescs[i].OriginalFirstThunk ) )
					: iaTable;

				// インポートシンボル列挙
				for ( UINT t = 0; 0 != inTable[t]; t ++ )
				{
					// INT のレコードがイメージ内に収まっているか
					if ( inTable[t] < getOptionalHeader()->SizeOfImage )
					{
						ImportSymbol symbol;
						
						symbol.iatEntryRva = importDescs[i].FirstThunk + sizeof( DWORD ) * t;
						symbol.iatEntry = &iaTable[t];
						symbol.address = iaTable[t];
						
						if ( IMAGE_SNAP_BY_ORDINAL32( inTable[t] ) )
						{
							// 序数によるインポート
							symbol.ordinal = static_cast< WORD >( IMAGE_ORDINAL32( inTable[t] ) );
						}
						else
						{
							// 名前によるインポート
							IMAGE_IMPORT_BY_NAME * byName;
							
							byName = static_cast< IMAGE_IMPORT_BY_NAME * >(
								_getImportData( inTable[t] ) );
							
							symbol.hint = byName->Hint;
							symbol.name = reinterpret_cast< LPSTR >( byName->Name );
						}

						symbols.insert( symbol );
					}
				}
			}
		}
	}

	return symbols;
}

//! インポートアドレステーブルを取得
dwords PeFile::getIat(	LPCSTR dllName ) const
{
	// NRVO is applied in release build.

	dwords iat;
	IMAGE_IMPORT_DESCRIPTOR * importDescs;

	importDescs = reinterpret_cast< IMAGE_IMPORT_DESCRIPTOR * >( getLoadImportBase() );
	
	if ( importDescs )
	{
		for ( UINT i = 0; 0 != importDescs[i].FirstThunk; i ++ )
		{
			LPCSTR name = static_cast< LPCSTR >( _getImportData( importDescs[i].Name ) );

			if ( NULL == dllName || 0 == _stricmp( dllName, name ) )
			{
				DWORD * iaTable = static_cast< DWORD * >( 
					_getIatData( importDescs[i].FirstThunk ) );
				
				for ( UINT t = 0; 0 != iaTable[t]; t ++ )
				{
					DWORD address;

					address = iaTable[t];

					if ( NULL != _mOriginalBase )
					{
						address +=  reinterpret_cast< DWORD >( _mOriginalBase );
					}

					iat.push_back( address );
				}
			}
		}
	}

	return iat;
}

//! エクスポートイメージファイルがロードされた時間を取得
DWORD PeFile::getExportImageFileLoadTime() const
{
	DWORD time;
	IMAGE_EXPORT_DIRECTORY * exportDir;

	time = 0;

	exportDir = reinterpret_cast< IMAGE_EXPORT_DIRECTORY * >( getLoadExportBase() );
	
	if ( exportDir )
	{
		time = exportDir->TimeDateStamp;
	}

	return time;
}

//! エクスポートイメージファイル名を取得
string PeFile::getExportImageFileName() const
{
	string name;
	IMAGE_EXPORT_DIRECTORY * exportDir;

	exportDir = reinterpret_cast< IMAGE_EXPORT_DIRECTORY * >( getLoadExportBase() );
	
	if ( exportDir )
	{
		name = static_cast< LPSTR >( _getExportData( exportDir->Name ) );
	}

	return name;
}

//! エクスポートシンボルの列挙を取得
ExportSymbolContainer PeFile::getExportSymbols() const
{
	// NRVO is applied in release build.

	ExportSymbolContainer symbols;
	IMAGE_EXPORT_DIRECTORY * exportDir;

	exportDir = reinterpret_cast< IMAGE_EXPORT_DIRECTORY * >(
		getLoadExportBase() );

	if ( exportDir )
	{
		for ( UINT i = 0; i < exportDir->NumberOfFunctions; i ++ )
		{
			ExportSymbol symbol;

			symbol.rva = _getExportSymbolRva( i );

			if ( symbol.rva )
			{
				symbol.address = _getExportSymbolAddress( symbol.rva );
				symbol.ordinal = i + exportDir->Base;
				symbol.name = _findExportSymbolName( i );
				symbol.forwardingDll = _isExportForwarded( symbol.rva )
					? static_cast< LPSTR >( _getExportData( symbol.rva ) ) : "";

				if ( !symbol.forwardingDll.empty() )
				{
					DWORD index = symbol.forwardingDll.find( ".", 0 ) + 1;

					if ( string::npos != index )
					{
						symbol.forwardingName = symbol.forwardingDll.substr( index ); 
						symbol.forwardingDll = symbol.forwardingDll.erase( index ) + "dll";
					}
				}

				symbols.insert( symbol );
			}
		}
	}

	return symbols;
}

//! 序数からエクスポートシンボルを取得
DWORD PeFile::getExportSymbolAddressFromOrdinal( WORD ordinal ) const
{
	DWORD address = 0;
	IMAGE_EXPORT_DIRECTORY * exportDir;

	exportDir = reinterpret_cast< IMAGE_EXPORT_DIRECTORY * >( getLoadExportBase() );

	if ( exportDir )
	{
		DWORD symbolRva = _getExportSymbolRva( ordinal - exportDir->Base );

		if ( symbolRva )
		{
			if ( !_isExportForwarded( symbolRva ) )
			{
				// 転送されていない場合

				address = _getExportSymbolAddress( symbolRva );
			}
#if 0
			else
			{
				// 転送されている場合

				LPSTR forwardedTo = static_cast< LPSTR >( _getExportData( symbolRva ) );
				// 転送先名は "NTDLL.RtlHeapAlloc" というように
				// DLL名.シンボル名 の形式になっているので、以下二つに分ける
				// dllname -> NTDLL.DLL symbolName -> RtHeapAlloc
				LPSTR symbolName = _tcsstr( forwardedTo, _T( "." ) ) + 1;
				TCHAR dllName[MAX_PATH] = { 0 };

				// DLL 名をコピーして拡張子を付与する
				memcpy( dllName, forwardedTo, symbolName - forwardedTo );
				_tcsncat_s( dllName, sizeof( dllName ), _T( "DLL" ), _TRUNCATE );

				HMODULE module = GetModuleHandle( dllName );

				if ( NULL == module )
				{
					module = LoadLibrary( dllName );
				}
				
				if ( module )
				{
					address = PeFile( module ).getExportSymbolAddressFromName( symbolName );
				}
			}
#endif
		}
	}

	return address;
}

//! エクスポート名からエクスポートシンボルを取得
DWORD PeFile::getExportSymbolAddressFromName( LPCSTR name ) const
{
	DWORD address = 0;
	IMAGE_EXPORT_DIRECTORY * exportDir;

	exportDir = reinterpret_cast< IMAGE_EXPORT_DIRECTORY * >( getLoadExportBase() );

	if ( exportDir )
	{
		DWORD * nameRvas = static_cast< DWORD * >(
			_getExportData( exportDir->AddressOfNames ) );
			
		WORD * indices = static_cast< WORD * >(
			_getExportData( exportDir->AddressOfNameOrdinals ) );

		// バイナリサーチ
		for ( UINT left = 0, right = exportDir->NumberOfNames - 1; left <= right; )
		{
			UINT middle = ( left + right ) / 2;
			LPSTR n = static_cast< LPSTR >( _getExportData( nameRvas[middle] ) );
			INT cmp = strcmp( name, n );

			if ( 0 == cmp )
			{
				// 発見

				address = getExportSymbolAddressFromOrdinal( 
					static_cast< WORD >( indices[middle] + exportDir->Base ) );

				break;
			}
			else if ( cmp > 0 )
			{
				left = middle + 1;
			}
			else
			{
				right = middle - 1;
			}
		}
	}

	return address;
}

//! エクスポートシンボルの名前を探す 
LPSTR PeFile::_findExportSymbolName( UINT index ) const
{
	LPSTR name = "";

	IMAGE_EXPORT_DIRECTORY * exportDir;

	exportDir = reinterpret_cast< IMAGE_EXPORT_DIRECTORY * >(
		getLoadExportBase() );

	if ( exportDir )
	{
		DWORD * nameRvas = static_cast< DWORD * >( 
			_getExportData( exportDir->AddressOfNames ) );
			
		WORD * indices = static_cast< WORD * >(
			_getExportData( exportDir->AddressOfNameOrdinals ) );

		for ( DWORD i = 0; i < exportDir->NumberOfNames; i ++ )
		{
			if ( indices[i] == index )
			{
				name = static_cast< LPSTR >( _getExportData( nameRvas[i] ) );
				break;
			}
		}
	}

	return name;
}

//! エクスポートシンボルの RVA を取得
DWORD PeFile::_getExportSymbolRva( UINT index ) const
{
	DWORD rva = 0;
	IMAGE_EXPORT_DIRECTORY * exportDir;

	exportDir = reinterpret_cast< IMAGE_EXPORT_DIRECTORY * >( getLoadExportBase() );

	if ( exportDir && 0 <= index && index <= exportDir->NumberOfFunctions )
	{
		rva = static_cast< DWORD * >( _getExportData( exportDir->AddressOfFunctions ) )[index];
	}

	return rva;
}

//! エクスポートシンボルが転送されているか
bool PeFile::_isExportForwarded( DWORD rva ) const
{
	bool forwarded = false;

	if ( getLoadBase() )
	{
		IMAGE_DATA_DIRECTORY & dirEntry =
			getOptionalHeader()->DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];

		forwarded = dirEntry.VirtualAddress && dirEntry.Size
			&& dirEntry.VirtualAddress <= rva
			&& rva < dirEntry.VirtualAddress + dirEntry.Size;
	}

	return forwarded;
}

//! ディレクトリエントリ取得
BYTE * PeFile::_getDirectoryEntry( UINT index ) const
{
	BYTE * data = NULL;

	if ( 0 < _getDirectoryEntrySize( index ) && _getDirectoryEntryRva( index ) )
	{
		data = getLoadBase() + _getDirectoryEntryRva( index );
	}

	return data;
}

//! ディレクトリエントリ RVA 取得
DWORD PeFile::_getDirectoryEntryRva( UINT index ) const
{
	DWORD rva = 0;
	
	if ( getLoadBase()
		&& 0 <= index 
		&& index < IMAGE_NUMBEROF_DIRECTORY_ENTRIES )
	{
		rva = getOptionalHeader()->DataDirectory[index].VirtualAddress;
	}
	
	return rva;
}

//! ディレクトリエントリサイズ取得
DWORD PeFile::_getDirectoryEntrySize( UINT index ) const
{
	DWORD size = 0;

	if ( getLoadBase()
		&& 0 <= index 
		&& index < IMAGE_NUMBEROF_DIRECTORY_ENTRIES )
	{
		size = getOptionalHeader()->DataDirectory[index].Size;
	}

	return size;
}

//! イメージディレクトリ内のデータを取得
LPVOID PeFile::_getImportData( DWORD rva ) const
{
	DWORD rvaFromImportBase;
	
	rvaFromImportBase = _getDirectoryEntryRva( IMAGE_DIRECTORY_ENTRY_IMPORT );
	
	if ( rva >= rvaFromImportBase )
	{
		rvaFromImportBase = rva - rvaFromImportBase;
	}
	
	return static_cast< LPVOID >( ( getLoadImportBase() + rvaFromImportBase ) );
}

//! エクスポートディレクトリ内のデータを取得
LPVOID PeFile::_getExportData( DWORD rva ) const
{
	DWORD rvaFromExportBase;
	
	rvaFromExportBase = _getDirectoryEntryRva( IMAGE_DIRECTORY_ENTRY_EXPORT );
	
	if ( rva >= rvaFromExportBase )
	{
		rvaFromExportBase = rva - rvaFromExportBase;
	}
	
	return static_cast< LPVOID >( getLoadExportBase() + rvaFromExportBase );
}

//! IAT ディレクトリ内のデータを取得
LPVOID PeFile::_getIatData( DWORD rva ) const
{
	DWORD rvaFromIatBase;
	
	rvaFromIatBase = _getDirectoryEntryRva( IMAGE_DIRECTORY_ENTRY_IAT );
	
	if ( rva >= rvaFromIatBase )
	{
		rvaFromIatBase = rva - rvaFromIatBase;
	}
	
	return static_cast< LPVOID >( getLoadIatBase() + rvaFromIatBase );
}

//! エクスポートシンボルのアドレスを取得
DWORD PeFile::_getExportSymbolAddress( DWORD rva ) const
{
	DWORD address = 0;
	
	if ( _mOriginalBase )
	{
		address = reinterpret_cast< DWORD >( _mOriginalBase + rva );
	}
	else
	{
		address = reinterpret_cast< DWORD >( getLoadBase() + rva );
	}
	
	return address;
}

//! イメージファイルをメモリにマッピング
BYTE * PeFile::_mappingImage( LPCTSTR fileName )
{
	BYTE * mapped = NULL;
	bytes fileData;

	if ( _loadFile( fileName, fileData ) )
	{
		// NT ヘッダの取得
		IMAGE_NT_HEADERS * nt = ImageNtHeader( &fileData[0] );
		
		if ( NULL == nt )
		{
			std::cerr << "[ERROR]/_mappingImage(): Not PE file format !" << std::endl;
		}
		else
		{
			// セクションテーブルへのポインタを取得
			IMAGE_SECTION_HEADER * sec = reinterpret_cast< IMAGE_SECTION_HEADER * >( nt + 1 );

			mapped = static_cast< BYTE * >(
				VirtualAlloc(
					NULL,
					nt->OptionalHeader.SizeOfImage,
					MEM_COMMIT,
					PAGE_READWRITE ) );

			if ( NULL == mapped )
			{
				std::cerr << "[ERROR]/_mappingImage(): Failed VirtualAlloc() !" << std::endl;
			}
			else
			{
				// ヘッダをコピー
				memcpy( mapped, &fileData[0], nt->OptionalHeader.SizeOfHeaders );

				// セクションデータをコピー
				for ( DWORD i = 0; i < nt->FileHeader.NumberOfSections; i ++ )
				{
					// セクションデータがある場合
					if ( sec[i].PointerToRawData )
					{
						memcpy( &mapped[sec[i].VirtualAddress],
							&fileData[sec[i].PointerToRawData],
							sec[i].SizeOfRawData );
					}
				}
			}
		}
	}

	return mapped;
}

//! 指定プロセスのモジュールイメージヘッダをメモリにマッピング
BYTE * PeFile::_mappingModuleImageHeader( 
	HANDLE processHandle, 
	LPVOID baseOffset )
{
	BYTE * mapped = NULL;
	BYTE * dosNtHeader;
	DWORD dosNtSize;
	
	dosNtSize = sizeof( IMAGE_DOS_HEADER ) + sizeof( IMAGE_NT_HEADERS );
	
	dosNtHeader = static_cast< BYTE * >(
		VirtualAlloc(
			NULL,
			dosNtSize,
			MEM_COMMIT,
			PAGE_READWRITE ) );
	
	if ( NULL == dosNtHeader )
	{
		std::cerr << "[ERROR]/_mappingModuleImageHeader(): Failed VirtualAlloc() !" << std::endl;
	}
	else
	{
		try
		{
			IMAGE_NT_HEADERS * nt;
				
			ReadMemory(
				processHandle, 
				baseOffset, 
				dosNtHeader, 
				dosNtSize );

			nt = ImageNtHeader( dosNtHeader );
			
			if ( NULL == nt )
			{
				std::cerr << "[ERROR]/_mappingModuleImageHeader(): Not PE file format !" << std::endl;
			}
			else
			{
				DWORD loadSize;
				
				loadSize = nt->OptionalHeader.SizeOfHeaders;
				
				mapped = static_cast< BYTE * >(
					VirtualAlloc(
						NULL,
						loadSize,
						MEM_COMMIT,
						PAGE_READWRITE ) );
				
				if ( NULL == mapped )
				{
					std::cerr << "[ERROR]/_mappingModuleImageHeader(): Failed VirtualAlloc() !" << std::endl;

					if ( 0 == loadSize )
					{
						std::cerr << "[ERROR]/_mappingModuleImageHeader(): PE header size is 0 !" << std::endl;
					}
				}
				else
				{
					try
					{
						// イメージのヘッダ全域を読む
						ReadMemory(
							processHandle, 
							baseOffset, 
							mapped, 
							loadSize );
					}
					catch ( ... )
					{
						VirtualFree( mapped, 0, MEM_RELEASE );
						
						mapped = NULL;
						
						throw;
					}
				}
			}
		}
		catch ( std::exception & e )
		{
			std::cerr << "[ERROR]/_mappingModuleImageHeader(): " << e.what() << " !" << std::endl;
		}

		VirtualFree( dosNtHeader, 0, MEM_RELEASE );
	}
	
	return mapped;
}

//! 指定プロセスのモジュールイメージディレクトリをメモリにマッピング
BYTE * PeFile::_mappingModuleImageDirectory(
	HANDLE processHandle, 
	LPVOID baseOffset,
	DWORD rva,
	DWORD size )
{
	BYTE * mapped;

	mapped = NULL;

	if ( 0 < size )
	{
		mapped = static_cast< BYTE * >(
			VirtualAlloc(
				NULL,
				size,
				MEM_COMMIT,
				PAGE_READWRITE ) );
		
		if ( NULL == mapped )
		{
			std::cerr << "[ERROR]/_mappingModuleImageDirectory(): PE header size is 0 !" << std::endl;
		}
		else
		{
			// インポートディレクトリを読む
			try
			{
				ReadMemory( 
					processHandle, 
					reinterpret_cast< LPVOID >(
					reinterpret_cast< DWORD >( baseOffset ) + rva ), 
					mapped, 
					size );
			}
			catch ( std::exception & e )
			{
				VirtualFree( mapped, 0, MEM_RELEASE );
				
				mapped = NULL;
				
				std::cerr << "[ERROR]/_mappingModuleImageDirectory(): " << e.what() << " !" << std::endl;
			}
		}
	}
	
	return mapped;
}

//! ファイルを std::vector<BYTE> に読み込む。
bool PeFile::_loadFile(
	LPCTSTR fileName,
	bytes & data )
{
	bool loaded = false;
	HANDLE fileHandle = INVALID_HANDLE_VALUE;
	LARGE_INTEGER fileSize = { 0 };
	DWORD readSize = 0;

	fileHandle = CreateFile(
		fileName,
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL );

	if( INVALID_HANDLE_VALUE == fileHandle )
	{
		std::cerr << "[ERROR]/_loadFile(): Failed CreateFile() !" << std::endl;
	}
	else
	{
		if( FALSE == GetFileSizeEx( fileHandle, &fileSize ) )
		{
			std::cerr << "[ERROR]/_loadFile(): Failed GetFileSizeEx() !" << std::endl;
		}
		else
		{
			if( fileSize.HighPart
				||  LONG_MAX < fileSize.LowPart
				||  0 == fileSize.LowPart )
			{
				std::cerr << "[ERROR]/_loadFile(): Illegal file size !" << std::endl;
			}
			else
			{
				data.resize( fileSize.LowPart, 0 );

				if( FALSE == ReadFile( fileHandle, &data[0], fileSize.LowPart, &readSize, NULL ) )
				{
					std::cerr << "[ERROR]/_loadFile(): Failed ReadFile() !" << std::endl;
				}
				else
				{
					loaded = true;
				}
			}
		}
		
		CloseHandle( fileHandle );
	}

	return loaded;
}
