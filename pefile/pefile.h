﻿/*!
 * \file	pefile.h
 *
 * \brief	PE ファイル
 *
 * \note
 *
 */

#pragma once

#include <windows.h>
#include <set>
#include <string>
#include <vector>

typedef std::string						string;			//!< 文字列
typedef std::wstring					wstring;		//!< マルチバイト文字列
typedef std::basic_string< TCHAR >		tstring;		//!< TCHAR 文字列
typedef std::vector< tstring >			tstrings;		//!< TCHAR 文字列コンテナ
typedef std::vector< string >			strings;		//!< 文字列コンテナ
typedef std::vector< BYTE >				bytes;			//!< バイト配列
typedef std::vector< DWORD >			dwords;			//!< DWORD 配列

/*!
 * \class	Uncopyable 
 *
 * \brief	複製禁止機構クラス
 */
class Uncopyable 
{
protected:
	/*！
	 * \fn			Uncopyable()
	 *
	 * \brief		デフォルトコンストラクタ
	 */
	Uncopyable()
	{
	}

	/*！
	 * \fn			~Uncopyable()
	 *
	 * \brief		デストラクタ
	 */
	~Uncopyable()
	{
	}

private:
	/*！
	 * \fn			Uncopyable( const Uncopyable & other )
	 *
	 * \brief		コピーコンストラクタ
	 *
	 * \note		禁止
	 */
	Uncopyable( const Uncopyable & other );

	/*！
	 * \fn			Uncopyable( const Uncopyable & other )
	 *
	 * \brief		コピー
	 *
	 * \note		禁止
	 *
	 * \param[in]	other			コピー元オブジェクト
	 * \return		コピー先オブジェクト
	 */
	Uncopyable & operator=( const Uncopyable & other );
};

/*!
 * \struct	_t_ImportSymbol
 *
 * \brief	インポートシンボル情報構造体
 */
struct _t_ImportSymbol
{
	DWORD iatEntryRva;							//!< IAT エントリの RVA
	DWORD * iatEntry;							//!< IAT エントリへのポインタ
	DWORD address;								//!< インポートシンボルへのポインタ
	WORD hint;									//!< ヒント
	WORD ordinal;								//!< 序数
	string name;								//!< 名前
	
	/*！
	 * \fn			operator<( const _t_ImportSymbol & other ) const
	 *
	 * \brief		インポートシンボル情報比較
	 *
	 * \param[in]	other			比較対象インポートシンボル情報右辺値
	 * \return		true			this < other
	 * \return		false			this >= other
	 */
	bool operator<( const _t_ImportSymbol & other ) const
	{
		return this->address < other.address;
	}
};
typedef struct _t_ImportSymbol ImportSymbol;

/*!
 * \struct	_t_ExportSymbol
 *
 * \brief	エクスポートシンボル情報構造体
 */
struct _t_ExportSymbol
{
	DWORD rva;									//!< エクスポートシンボルの RVA
	DWORD address;								//!< エクスポートシンボルへのポインタ
	DWORD ordinal;								//!< 序数
	string name;								//!< 名前
	string forwardingDll;						//!< 転送先 Dll
	string forwardingName;						//!< 転送先での名前

	/*！
	 * \fn			operator<( const _t_ExportSymbol & other ) const
	 *
	 * \brief		エクスポートシンボル情報比較
	 *
	 * \param[in]	other			比較対象エクスポートシンボル情報右辺値
	 * \return		true			this < other
	 * \return		false			this >= other
	 */
	bool operator<( const _t_ExportSymbol & other ) const
	{
		return this->address < other.address;
	}
};
typedef struct _t_ExportSymbol ExportSymbol;

/*!
 * \struct	_t_SectionData
 *
 * \brief	セクションデータ構造体
 */
struct _t_SectionData
{
	DWORD address;								//!< セクションアドレス
	DWORD size;									//!< セクションサイズ
	bool existInitData;							//!< 初期データの有無フラグ: true - 有 false - 無
	string name;								//!< セクション名
};
typedef struct _t_SectionData SectionData;

typedef std::vector< SectionData >			SectionDataContainer;
typedef std::set< ImportSymbol >			ImportSymbolContainer;
typedef std::set< ExportSymbol >			ExportSymbolContainer;

/*!
 * \class	PeFile
 *
 * \brief	PE ファイルクラス
 */
class PeFile
	: private Uncopyable
{
public:
	/*！
	 * \fn			PeFile( LPCTSTR fileName, DWORD originalBase = 0 )
	 *
	 * \brief		コンストラクタ
	 *
	 * \param[in]	fileName				ファイル名
	 * \param[in]	originalBase			オリジナルベースアドレス (デフォルト 0)
	 */
	PeFile(
		LPCTSTR fileName,
		DWORD originalBase = 0 );

	//! コンストラクタ (モジュールハンドル)

	/*！
	 * \fn			PeFile( HMODULE hModule )
	 *
	 * \brief		コンストラクタ
	 *
	 * \param[in]	hModule					モジュールハンドル
	 */
	explicit PeFile( HMODULE hModule );

	//! コンストラクタ (プロセスハンドル, ベースオフセット)
	/*！
	 * \fn			PeFile( HANDLE processHandle, DWORD baseOffset )
	 *
	 * \brief		コンストラクタ
	 *
	 * \param[in]	processHandle			プロセスハンドル
	 * \param[in]	baseOffset				ベースオフセット
	 */
	PeFile( 
		HANDLE processHandle, 
		DWORD baseOffset );
	
	/*！
	 * \fn			~PeFile()
	 *
	 * \brief		デストラクタ
	 */
	virtual ~PeFile();

	/*！
	 * \fn			getLoadBase() const
	 *
	 * \brief		ロードアドレス取得
	 *
	 * \return		ロードアドレス
	 */
	BYTE * getLoadBase() const;

	/*！
	 * \fn			getLoadImportBase() const
	 *
	 * \brief		ロードインポートアドレス取得
	 *
	 * \return		ロードインポートアドレス
	 */
	BYTE * getLoadImportBase() const;

	/*！
	 * \fn			getLoadExportBase() const
	 *
	 * \brief		ロードエクスポートアドレス取得
	 *
	 * \return		ロードエクスポートアドレス
	 */
	BYTE * getLoadExportBase() const;

	/*！
	 * \fn			getLoadIatBase() const
	 *
	 * \brief		ロード IAT アドレス取得
	 *
	 * \return		ロード IAT アドレス
	 */
	BYTE * getLoadIatBase() const;
	
	/*！
	 * \fn			getImageSize() const
	 *
	 * \brief		イメージサイズ取得
	 *
	 * \return		イメージサイズ
	 */
	DWORD getImageSize() const;

	/*！
	 * \fn			getNtHeaders() const
	 *
	 * \brief		NT ヘッダ取得
	 *
	 * \return		NT ヘッダ
	 */
	IMAGE_NT_HEADERS * getNtHeaders() const;
	
	/*！
	 * \fn			getFileHeader() const
	 *
	 * \brief		ファイルヘッダ取得
	 *
	 * \return		ファイルヘッダ
	 */
	IMAGE_FILE_HEADER * getFileHeader() const;

	/*！
	 * \fn			getOptionalHeader() const
	 *
	 * \brief		オプショナルヘッダ取得
	 *
	 * \return		オプショナルヘッダ
	 */
	IMAGE_OPTIONAL_HEADER * getOptionalHeader() const;

	/*！
	 * \fn			getSectionHeader() const
	 *
	 * \brief		セクションヘッダ取得
	 *
	 * \return		セクションヘッダ
	 */
	IMAGE_SECTION_HEADER * getSectionHeader( UINT index ) const;

	/*！
	 * \fn			getSectionData() const
	 *
	 * \brief		セクションデータ取得
	 *
	 * \param[out]	sections				セクションデータコンテナ
	 */
	SectionDataContainer getSectionData() const;

	/*！
	 * \fn			getSectionDataFromName( LPCSTR name ) const
	 *
	 * \brief		指定した名前のセクションデータを取得
	 *
	 * \param[in]	name					指定した名前
	 * \return		セクションデータ
	 */
	SectionData getSectionDataFromName( LPCSTR name ) const;

	//! EXE のテキストセクションデータを取得
	/*！
	 * \fn			getExeTextSectionData() const
	 *
	 * \brief		EXE テキストセクションデータを取得
	 *
	 * \return		EXE テキストセクションデータ
	 */
	SectionData getExeTextSectionData() const;

	/*！
	 * \fn			getImportDllNames() const
	 *
	 * \brief		インポート DLL 名の列挙を取得
	 *
	 * \param[out]	dllNames				DLL名コンテナ
	 */
	strings getImportDllNames() const;
	
	/*！
	 * \fn			getImportSymbols( LPCSTR dllName ) const
	 *
	 * \brief		インポートしているシンボルの列挙を取得
	 *
	 * \param[in]	dllName					DLL名
	 * \param[out]	symbols					インポートシンボルコンテナ
	 */
	ImportSymbolContainer getImportSymbols( LPCSTR dllName ) const;
	
	/*！
	 * \fn			getIat( LPCSTR dllName )
	 *
	 * \brief		IAT を取得
	 *
	 * \param[out]	iat					IAT
	 * \param[in]	dllName				DLL名
	 */
	dwords getIat( LPCSTR dllName ) const;

	/*！
	 * \fn			getExportImageFileLoadTime() const
	 *
	 * \brief		エクスポートイメージファイルがロードされた時間を取得
	 *
	 * \return		エクスポートイメージファイルがロードされた時間
	 */
	DWORD getExportImageFileLoadTime() const;

	/*！
	 * \fn			getExportImageFileName() const
	 *
	 * \brief		エクスポートイメージファイル名を取得
	 *
	 * \return		エクスポートイメージファイル名
	 */
	string getExportImageFileName() const;

	/*！
	 * \fn			getExportSymbols() const
	 *
	 * \brief		エクスポートシンボルの列挙を取得
	 *
	 * \param[out]	symbols				エクスポートシンボルコンテナ
	 */
	ExportSymbolContainer getExportSymbols() const;

	/*！
	 * \fn			getExportSymbolAddressFromOrdinal( WORD ordinal ) const
	 *
	 * \brief		序数からエクスポートシンボルアドレスを取得
	 *
	 * \param[in]	ordinal				序数
	 * \return		エクスポートシンボルアドレス
	 */
	DWORD getExportSymbolAddressFromOrdinal( WORD ordinal ) const;
	//! エクスポート名からエクスポートシンボルを取得
	DWORD getExportSymbolAddressFromName( LPCSTR name ) const;

private:
	enum
	{
		FLAT_ALLOCATE,				//!< 連続領域アロケート
		DISCRETE_ALLOCATE,			//!< 不連続アロケート
		NO_ALLOCATE					//!< アロケート無し
	};

private:
	/*！
	 * \fn			_findExportSymbolName( UINT index ) const
	 *
	 * \brief		エクスポートシンボル名検索
	 *
	 * \param[in]	index				インデックス
	 * \return		エクスポートシンボル名
	 */
	LPSTR _findExportSymbolName( UINT index ) const;
	
	/*！
	 * \fn			_getExportSymbolRva( UINT index ) const
	 *
	 * \brief		エクスポートシンボルの RVA を取得
	 *
	 * \param[in]	index				インデックス
	 * \return		エクスポートシンボルの RVA
	 */
	DWORD _getExportSymbolRva( UINT index ) const;

	//! エクスポートシンボルが転送されているか
	/*！
	 * \fn			 _isExportForwarded( DWORD rva ) const
	 *
	 * \brief		エクスポートシンボルが転送されているかを判定
	 *
	 * \param[in]	rva					RVA
	 * \return		true				転送されている
	 * \return		false				転送されていない
	 */
	bool _isExportForwarded( DWORD rva ) const;

	/*！
	 * \fn			_getDirectoryEntry( UINT index ) const
	 *
	 * \brief		ディレクトリエントリ取得
	 *
	 * \param[in]	index				インデックス
	 * \return		ディレクトリエントリ
	 */
	BYTE * _getDirectoryEntry( UINT index ) const;

	/*！
	 * \fn			_getDirectoryEntryRva( UINT index ) const
	 *
	 * \brief		ディレクトリエントリ RVA 取得
	 *
	 * \param[in]	index				インデックス
	 * \return		ディレクトリエントリ RVA
	 */
	DWORD _getDirectoryEntryRva( UINT index ) const;

	/*！
	 * \fn			_getDirectoryEntrySize( UINT index ) const
	 *
	 * \brief		ディレクトリエントリサイズ取得
	 *
	 * \param[in]	index				インデックス
	 * \return		ディレクトリエントリサイズ
	 */
	DWORD _getDirectoryEntrySize( UINT index ) const;

	/*！
	 * \fn			_getImportData( DWORD rva ) const
	 *
	 * \brief		インポートディレクトリ内のデータを取得
	 *
	 * \param[in]	rva					RVA
	 * \return		インポートディレクトリ内のデータ
	 */
	LPVOID _getImportData( DWORD rva ) const;

	/*！
	 * \fn			_getExportData( DWORD rva ) const
	 *
	 * \brief		エクスポートディレクトリ内のデータを取得
	 *
	 * \param[in]	rva					RVA
	 * \return		エクスポートディレクトリ内のデータ
	 */
	LPVOID _getExportData( DWORD rva ) const;

	/*！
	 * \fn			_getIatData( DWORD rva ) const
	 *
	 * \brief		IAT ディレクトリ内のデータを取得
	 *
	 * \param[in]	rva					RVA
	 * \return		IAT ディレクトリ内のデータ
	 */
	LPVOID _getIatData( DWORD rva ) const;

	/*！
	 * \fn			_getExportSymbolAddress( DWORD rva ) const
	 *
	 * \brief		エクスポートシンボルのアドレスを取得
	 *
	 * \param[in]	rva					RVA
	 * \return		エクスポートシンボルのアドレス
	 */
	DWORD _getExportSymbolAddress( DWORD rva ) const;
	
	/*！
	 * \fn			_mappingImage( LPCTSTR fileName )
	 *
	 * \brief		イメージファイルをメモリにマッピング
	 *
	 * \param[in]	fileName			イメージファイル名
	 * \return		マッピングされたメモリポインタ
	 */
	static BYTE * _mappingImage( LPCTSTR fileName );
	
	/*！
	 * \fn			_mappingModuleImageHeader( HANDLE processHandle, LPVOID baseOffset )
	 *
	 * \brief		指定プロセスのモジュールイメージヘッダをメモリにマッピング
	 *
	 * \param[in]	processHandle		プロセスハンドル
	 * \param[in]	baseOffset			ベースオフセット
	 * \return		マッピングされたメモリポインタ
	 */
	static BYTE * _mappingModuleImageHeader( 
		HANDLE processHandle, 
		LPVOID baseOffset );
	
	/*！
	 * \fn			_mappingModuleImageDirectory( HANDLE processHandle, LPVOID baseOffset, DWORD rva, DWORD size )
	 *
	 * \brief		指定プロセスのモジュールイメージヘッダをメモリにマッピング
	 *
	 * \param[in]	processHandle		プロセスハンドル
	 * \param[in]	baseOffset			ベースオフセット
	 * \param[in]	rva					RVA
	 * \param[in]	size				サイズ
	 * \return		マッピングされたメモリポインタ
	 */
	static BYTE * _mappingModuleImageDirectory(
		HANDLE processHandle, 
		LPVOID baseOffset,
		DWORD rva,
		DWORD size );
	
	//! ファイルを std::vector<BYTE> に読み込む。
	/*！
	 * \fn			_loadFile( LPCTSTR fileName, bytes & data )
	 *
	 * \brief		ロードファイル
	 *
	 * \param[in]	fileName			ファイル名
	 * \param[out]	data				ファイル内容バイト配列
	 * \return		true				成功
	 * \return		false				失敗
	 */
	static bool _loadFile(
		LPCTSTR fileName,
		bytes & data );

private:
	BYTE _mAllocateState;						//!< 内部アロケート状態
	bool _mIsLoaded;							//!< ロードフラグ
	BYTE * _mOriginalBase;						//!< オリジナルベースアドレス
	BYTE * _mLoadBase;							//!< ロードアドレス
	BYTE * _mLoadImportBase;					//!< ロードインポートアドレス
	BYTE * _mLoadExportBase;					//!< ロードエクスポートアドレス
	BYTE * _mLoadIatBase;						//!< ロード IAT アドレス	
	IMAGE_NT_HEADERS * _mNtHeaders;				//!< NT ヘッダー
	IMAGE_SECTION_HEADER * _mSectionTable;		//!< セクションヘッダーテーブル
};
